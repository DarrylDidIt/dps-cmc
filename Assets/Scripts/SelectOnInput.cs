﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {

    public EventSystem eventSystem;
    public GameObject selectedGameObject;

    private bool buttonSelected;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (System.Math.Abs(Input.GetAxisRaw("Horizontal")) > 0 && buttonSelected == false) {
            eventSystem.SetSelectedGameObject(selectedGameObject);
            buttonSelected = true;
        }
	}

    private void OnDisable()
    {
      buttonSelected = false;
    }
}
