﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HandleTurns {

    public string Attacker; //Name of attacker
    public string Type;
    public GameObject AttackersGameObject; // who is attacking
    public GameObject AttackersTarget; // who is going to be attacked

    //which attack is performed;
}
