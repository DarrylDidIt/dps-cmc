﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey("w")) {
			Debug.Log("Pressing W Key");
		}
		if(Input.GetButtonDown("Fire1")) {
			Debug.Log("Pressing Fire1 Key");
		}
		if(Input.GetKey("a")) {
			Debug.Log("Pressing a Key");
		}
		if(Input.GetMouseButton(0)) { 
			Debug.Log("Pressing Right Mouse Button");
		}
		if(Input.GetMouseButton(1)) { 
			Debug.Log("Pressing Left Mouse Button");
		}
	}
}
