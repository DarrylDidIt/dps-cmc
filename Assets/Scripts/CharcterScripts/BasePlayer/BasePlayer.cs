﻿using UnityEngine;

public class BasePlayer : MonoBehaviour{

    public string playerName;
    private int PlayerHealth;
    private int PlayerEnergy;

    readonly int Mind; //Player Accuracy
    readonly int Strength;
    readonly int Essence;
    readonly int Speed;
    readonly int Will;
    readonly int Body;
    readonly int Soul;
    readonly int Ultima;

    readonly int Gold; // In game Currency

    readonly int CurrentXP;
    readonly int RequiredXP;
    readonly int StatPointsToAllacate;

    public string playerHealth { get; set; }
    public int playerEnergy { get; set; }

    public string PlayerName{get;set;}
    public int currentXP{get;set;}
    public int requiredXP{get;set;}
    public int statPointsToAllacate{get;set;}

    public int PlayerLevel { get; set; }

    public BaseCharacterClass PlayerClass { get; set; }

    public BaseCharacterRace PlayerRace { get; set; }

    public int mind{get;set;}
    public int strength{get;set;}
    public int essence{get;set;}
    public int speed{get;set;}
    public int will{get;set;}
    public int body{get;set;}
    public int soul{get;set;}
    public int ultima{get;set;}
    public int gold{get;set;}
}
