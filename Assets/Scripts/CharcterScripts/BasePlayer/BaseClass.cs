using UnityEngine;
using System.Collections;

public class BaseClass
{
    //stats
    public int mind;
    public int strength;
    public int essence;
    public int speed;
    public int will;
    public int body;
    public int soul;
    public int ultima;

    public int Mind { get; set; }
    public int Strength { get; set; }
    public int Essence { get; set; }
    public int Speed { get; set; }
    public int Will { get; set; }
    public int Body { get; set; }
    public int Soul { get; set; }
    public int Ultima { get; set; }
}
