﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDevastatorClass : BaseCharacterClass {

	public  BaseDevastatorClass() {
        CharacterClassName = "Devastator";
        CharacterClassDescription = "Tech Melee and support" ;

		//Chacrtater Stats
		Mind = 2; 
		Strength = 3;
		Essence = 2;
		Speed = 3;
		Will = 3;
		Body = 4;
		Soul = 2;
		Ultima = 2;
 	}

	public static implicit operator string(BaseDevastatorClass v)
	{
		throw new NotImplementedException();
	}
}
