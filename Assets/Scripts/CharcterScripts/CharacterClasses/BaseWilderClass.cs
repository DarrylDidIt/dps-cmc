﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWilderClass : BaseCharacterClass {

	public  BaseWilderClass() {
        CharacterClassName = "Wilder";
        CharacterClassDescription = "All-arounder";

		//Chacrtater Stats
		Mind = 2; 
		Strength = 2;
		Essence = 2;
		Speed = 2;
		Will = 2;
		Body = 3;
		Soul = 3;
		Ultima = 3;
 	}

	public static implicit operator string(BaseWilderClass v)
	{
		throw new NotImplementedException();
	}
}
