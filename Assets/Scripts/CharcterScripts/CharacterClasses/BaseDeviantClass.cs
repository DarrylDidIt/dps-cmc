﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDeviantClass : BaseCharacterClass {

	public  BaseDeviantClass() {
        CharacterClassName = "Deviant";
        CharacterClassDescription = "Melee, control";

		//Chacrtater Stats
		Mind = 3; 
		Strength = 2;
		Essence = 1;
		Speed = 4;
		Will = 1;
		Body = 1;
		Soul = 3;
		Ultima = 2;
 	}

	public static implicit operator string(BaseDeviantClass v)
	{
		throw new NotImplementedException();
	}
}
