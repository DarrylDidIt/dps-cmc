﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBlazerClass : BaseCharacterClass {

	public  BaseBlazerClass() {
        CharacterClassName = "Blazer";
        CharacterClassDescription = "ranged damage";

		//Chacrtater Stats
		Mind = 5; 
		Strength = 1;
		Essence = 1;
		Speed = 5;
		Will = 2;
		Body = 2;
		Soul = 2;
		Ultima = 2;
 	}

	public static implicit operator string(BaseBlazerClass v)
	{
		throw new NotImplementedException();
	}
}
