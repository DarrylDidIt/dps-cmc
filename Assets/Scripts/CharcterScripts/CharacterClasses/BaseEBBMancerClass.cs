﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEBBMancerClass : BaseCharacterClass
{

    public BaseEBBMancerClass()
    {
        CharacterClassName = "EBB Mancer";
        CharacterClassDescription = "control magic";
		//Chacrtater Stats
		Mind = 4;
        Strength = 1;
        Essence = 3;
        Speed = 2;
        Will = 2;
        Body = 2;
        Soul = 3;
        Ultima = 1;
    }

	public static implicit operator string(BaseEBBMancerClass v)
	{
		throw new NotImplementedException();
	}
}
