﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDivinerClass : BaseCharacterClass {

	public  BaseDivinerClass() {
        CharacterClassName = "Diviner";
        CharacterClassDescription = "Tank by summons, Heals and support";

		//Chacrtater Stats
		Mind = 2; 
		Strength = 1;
		Essence = 3;
		Speed = 4;
		Will = 3;
		Body = 2;
		Soul = 2;
		Ultima = 2;
 	}

	public static implicit operator string(BaseDivinerClass v)
	{
		throw new NotImplementedException();
	}
}
