﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHeraldClass : BaseCharacterClass {

public  BaseHeraldClass() {
        CharacterClassName = "Herald";
        CharacterClassDescription = "Tank by summons, Heals and support";

		//Chacrtater Stats
		Mind = 2;
		Strength = 3;
		Essence = 3;
		Speed = 4;
		Will = 4;
		Body = 4;
		Soul = 2;
		Ultima = 2;
 	}

	public static implicit operator string(BaseHeraldClass v)
	{
		throw new NotImplementedException();
	}
}
