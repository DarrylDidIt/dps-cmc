﻿using System;
using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class BaseCharacterClass {

    public string characterClassName;
    public string characterClassDescription;
    //stats
    public int mind; 
    public int strength;
    public int essence;
    public int speed;
    public int will;
    public int body;
    public int soul;
    public int ultima;

    public enum CharacterClasses {
        Herald,
        Blazer,
        Deviant,
        Disciple,
        ArcaneBlade,
        Caster,
        EBBMancer,
        Diviner,
        Wilder,
        Devastator,
        Interfacer,
        Nova
    }

    public string CharacterClassName
    {
        get { return characterClassName; }
        set { characterClassName = value; }
    }

	public string CharacterClassDescription 
    {
        get { return characterClassDescription; }
        set { characterClassDescription = value; }
    }

    public CharacterClasses CharacterClass { get; set; }
    public List<BaseAbility> playerAbilites = new List<BaseAbility>();


	public int Mind 
    {
        get { return mind; }
        set { mind = value; }
    }

	public int Strength
    {
        get { return strength; }
        set { strength = value; }
    }
	public int Essence
    {
        get { return essence; }
        set { essence = value; }
    }
	public int Speed
    {
        get { return speed; }
        set { speed = value; }
    }
	public int Will
    {
        get { return will; }
        set { will = value; }
    }
	public int Body
    {
        get { return body; }
        set { body = value; }
    }
	public int Soul
    {
        get { return soul; }
        set { soul = value; }
    }
	public int Ultima
    {
        get { return ultima; }
        set { ultima = value; }
    }
}
