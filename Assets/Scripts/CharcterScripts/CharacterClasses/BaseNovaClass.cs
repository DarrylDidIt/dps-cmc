﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseNovaClass : BaseCharacterClass {

	public  BaseNovaClass() {
        CharacterClassName = "Nova";
        CharacterClassDescription = "Ranged damage, and support";

		//Chacrtater Stats
		Mind = 4; 
		Strength = 1;
		Essence = 2;
		Speed = 2;
		Will = 2;
		Body = 1;
		Soul = 5;
		Ultima = 1;
 	}

	public static implicit operator string(BaseNovaClass v)
	{
		throw new NotImplementedException();
	}
}
