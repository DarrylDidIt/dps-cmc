﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCasterClass : BaseCharacterClass {

	public  BaseCasterClass() {
        CharacterClassName = "Caster";
        CharacterClassDescription = "magic ranged damage";

		//Chacrtater Stats
		Mind = 4; 
		Strength = 1;
		Essence = 4;
		Speed = 1;
		Will = 1;
		Body = 3;
		Soul = 3;
		Ultima = 2;
 	}

	public static implicit operator string(BaseCasterClass v)
	{
		throw new NotImplementedException();
	}
}
