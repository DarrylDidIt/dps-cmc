﻿using System;

public class BaseArcaneBladeClass : BaseCharacterClass {

	public  BaseArcaneBladeClass() {
        CharacterClassName = "Arcane Blade";
        CharacterClassDescription = "Kaedyr Melee";

		//Chacrtater Stats
		Mind = 1; 
		Strength = 3;
		Essence = 2;
		Speed = 3;
		Will = 2;
		Body = 3;
		Soul = 3;
		Ultima = 2;

        //Class Abilities
        playerAbilites.Add(new BasicAttackAbility());
        playerAbilites.Add(new SwordSlash());
 	}
}
