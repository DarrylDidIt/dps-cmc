﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWeyevRace : BaseCharacterRace {

	public BaseWeyevRace() {
        RaceName = "Weyev";
        RaceDescription = "Weyev";

        //Race Stats
		//Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
