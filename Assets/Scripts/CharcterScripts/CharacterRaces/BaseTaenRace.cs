﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTaenRace : BaseCharacterRace {

	public BaseTaenRace() {
        RaceName = "Taen";
        RaceDescription = "Taen";

		//Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
