﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseShalnickRace : BaseCharacterRace {

	public BaseShalnickRace() {
        RaceName = "Shalnick";
        RaceDescription = "Shalnick";

		//Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
