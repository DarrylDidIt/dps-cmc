﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMaroMaroRace : BaseCharacterRace {

	public BaseMaroMaroRace() {
        RaceName = "Maron Maron";
        RaceDescription = "Taen";

		//Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
