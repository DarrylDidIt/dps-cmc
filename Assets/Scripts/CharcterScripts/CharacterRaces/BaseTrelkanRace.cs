﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTrelkanRace : BaseCharacterRace {

	public BaseTrelkanRace() {
        RaceName = "Trelkan";
        RaceDescription = "Trelkan";

		//Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
