﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseHaffenvalkRace : BaseCharacterRace {

	public BaseHaffenvalkRace() {
        RaceName = "Haffenvalk";
        RaceDescription = "Haffenvalk";

		//Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
