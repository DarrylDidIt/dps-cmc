﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDraazenRace : BaseCharacterRace {

	public BaseDraazenRace()
	{
		var baseCharacterRace = new BaseCharacterRace();

		RaceName = "Draazen";
        RaceDescription = "Draazen";

        //Race Stats
		HasMindBonus = true; 
		HasStrengthBonus = true;
		HasEssenceBonus = true;
		HasSpeedBonus = true;
		HasWillBonus = true;
		HasBodyBonus = true;
		HasSoulBonus = true;
		HasUltimaBonus = true;
    }
}
