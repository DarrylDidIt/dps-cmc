﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCharacterRace {

	private string raceName = "Needs a Name";
	private string raceDescription = "Needs a Decription";

    public string RaceName
    {
        get { return raceName; }
        set { raceName = value; }
    }

    public string RaceDescription
    {
        get { return raceDescription; }
        set { raceDescription = value; }
    }


	public bool HasMindBonus{ get;set; }
	public bool HasSpeedBonus{ get;set; }
	public bool HasWillBonus{ get;set; }
	public bool HasEssenceBonus{ get;set; }
	public bool HasBodyBonus{ get;set; }
	public bool HasSoulBonus{ get;set; }
	public bool HasUltimaBonus{ get;set; }
	public bool HasStrengthBonus{ get;set; }
}
