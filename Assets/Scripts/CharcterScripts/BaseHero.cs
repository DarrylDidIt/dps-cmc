﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseHero : MonoBehaviour {
	public new string name;

	public float baseHP;
	public float curHP;

	public float baseMP;
	public float curMP;

	public float baseUltima;
	public float curUltima;

	// Character Race

	// Character Class

	public int mind; 
	public int strength;
	public int essence;
	public int speed;
	public int will;
	public int body;
	public int soul;
	public int ulitma;
}
