﻿using SavingAndLoading;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CreateNewCharacter : MonoBehaviour {

    private BasePlayer newPlayer;
    private bool isHeraldClass;
	private bool isBlazerClass;
	private bool isDeviantClass;
	private bool isDiscipleClass;
	private bool isDivinerClass;
	private bool isWilderClass;
	private bool isArcaneBladeClass;
	private bool isCasterClass;
	private bool isEBBMancerClass;
	private bool isDevastatorClass;
	private bool isInterfacerClass;
	private bool isNovaClass;
	private string playerName = "Enter Name";

	// Use this for initialization
	void Start () {
		newPlayer = new BasePlayer();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI() {
		playerName = GUILayout.TextArea(playerName,15);
		isHeraldClass = GUILayout.Toggle(isHeraldClass, "Herald");
		isBlazerClass = GUILayout.Toggle(isBlazerClass, "Blazer");
		isDeviantClass = GUILayout.Toggle(isDeviantClass, "Deviant");
		isDiscipleClass = GUILayout.Toggle(isDiscipleClass, "Disciple");
		isDivinerClass = GUILayout.Toggle(isDivinerClass, "Diviner");
		isWilderClass = GUILayout.Toggle(isWilderClass, "Wilder");
		isArcaneBladeClass = GUILayout.Toggle(isArcaneBladeClass, "Arcane Blade");
		isCasterClass = GUILayout.Toggle(isCasterClass, "Caster");
		isEBBMancerClass = GUILayout.Toggle(isEBBMancerClass, "EBB Mancer");
		isNovaClass = GUILayout.Toggle(isNovaClass, "Nova");
		isDevastatorClass = GUILayout.Toggle(isDevastatorClass, "Devastator");
		isInterfacerClass = GUILayout.Toggle(isInterfacerClass, "Interfacer");
		

		if(GUILayout.Button("Create")){
			if(isHeraldClass) {
                newPlayer.PlayerClass = new BaseHeraldClass();
			} else if (isBlazerClass){
                newPlayer.PlayerClass = new BaseBlazerClass();
			} else if (isDeviantClass){
                newPlayer.PlayerClass = new BaseDeviantClass();
			} else if (isDiscipleClass){
                newPlayer.PlayerClass = new BaseDiscipleClass();
			} else if (isDivinerClass){
                newPlayer.PlayerClass = new BaseDivinerClass();
			} else if (isWilderClass){
                newPlayer.PlayerClass = new BaseWilderClass();
			} else if (isArcaneBladeClass){
                newPlayer.PlayerClass = new BaseArcaneBladeClass();
			} else if (isCasterClass){
                newPlayer.PlayerClass = new BaseCasterClass();
			} else if (isEBBMancerClass){
                newPlayer.PlayerClass = new BaseEBBMancerClass();
			} else if (isNovaClass){
                newPlayer.PlayerClass = new BaseNovaClass();
			} else if (isDevastatorClass){
                newPlayer.PlayerClass = new BaseDevastatorClass();
			} else if (isInterfacerClass){
                newPlayer.PlayerClass = new BaseInterfacerClass();
			}
			CreateNewPlayer();
			StoreNewPlayerInfo();

			SaveInformation.SaveAllInformation();
		}

		if(GUILayout.Button("Load")){
			SceneManager.LoadScene("Test");
		}
	}

	private void StoreNewPlayerInfo() {
		GameInformation.PlayerName = newPlayer.PlayerName;
        GameInformation.PlayerLevel = newPlayer.PlayerLevel;
        GameInformation.CharacterClassName = newPlayer.PlayerClass;
		GameInformation.Mind = newPlayer.mind;
		GameInformation.Strength = newPlayer.strength;
		GameInformation.Essence = newPlayer.essence;
		GameInformation.Speed = newPlayer.speed;
		GameInformation.Will = newPlayer.will;
		GameInformation.Body = newPlayer.body;
		GameInformation.Soul = newPlayer.soul;
		GameInformation.Ultima = newPlayer.ultima;
		GameInformation.Gold = newPlayer.gold;
	}

	private void CreateNewPlayer() {
		newPlayer.PlayerLevel = 1;
		newPlayer.PlayerName = playerName;
		newPlayer.gold = 1000;

        newPlayer.mind = newPlayer.PlayerClass.Mind;
        newPlayer.strength = newPlayer.PlayerClass.Strength;
        newPlayer.essence = newPlayer.PlayerClass.Essence;
        newPlayer.speed = newPlayer.PlayerClass.Speed;
        newPlayer.will = newPlayer.PlayerClass.Will;
        newPlayer.body = newPlayer.PlayerClass.Body;
        newPlayer.soul = newPlayer.PlayerClass.Soul;
        newPlayer.ultima = newPlayer.PlayerClass.Ultima;
		Debug.Log("Player Name:" + newPlayer.PlayerName);
        Debug.Log("Player Class:" + newPlayer.PlayerClass);
		Debug.Log("Player Level:" + newPlayer.PlayerLevel);
		Debug.Log("Player Will:" + newPlayer.will);
		Debug.Log("Player Strength:" + newPlayer.strength);
		Debug.Log("Player Essence:" + newPlayer.essence);
		Debug.Log("Player Speed:" + newPlayer.speed);
		Debug.Log("Player Mind:" + newPlayer.mind);
		Debug.Log("Player Body:" + newPlayer.body);
		Debug.Log("Player Soul:" + newPlayer.soul);
		Debug.Log("Player Ultima:" + newPlayer.ultima);
	}
}
