﻿using SavingAndLoading;
using UnityEngine;

public class DisplayCreatePlayerFunctions : MonoBehaviour{

	private int classSelection;
	private string[] classSelectionNames = { "Herald", "Blazer", "Deviant", "Disciple", "Arcane Blade", "Caster", "EBB Mancer", "Diviner", "Wilder", "Devastator", "Interfacer", "Nova" };
	private string playerName = "Enter Name";
	private string playerBio = "Enter Player Bio";
	//private bool isMale = true;
	private int genderSelection;
	private string[] genderTypes = { "Male", "Female"};

    private readonly StatAllocationModule statAllocationModule = new StatAllocationModule();

    //private int raceSelection;
    private string[] raceSelectionNames = { "Taen", "Maro Maro", "Draazen", "Weyev", "Trelkan", "Shalnick", "Haffenvalk" };

	public void DisplayClassSelections() {
		// a list of toggle button and and each will be a different class
		classSelection = GUI.SelectionGrid(new Rect(50, 50, 250, 300), classSelection, classSelectionNames, 3);
		 GUI.Label(new Rect(450, 50, 250, 300), FindClassDescription(classSelection));
		 GUI.Label(new Rect(450, 100, 250, 300), FindClassStatValue(classSelection));
	}

	public string FindClassDescription(int classSelection) {
        switch (classSelection)
        {
            case 0:
                BaseCharacterClass tempHeraldClass = new BaseHeraldClass();
                return tempHeraldClass.CharacterClassDescription;
            case 1:
                BaseCharacterClass tempBlazerClass = new BaseBlazerClass();
                return tempBlazerClass.characterClassDescription;
            case 2:
                BaseCharacterClass tempDeviantClass = new BaseDeviantClass();
                return tempDeviantClass.characterClassDescription;
            case 3:
                BaseCharacterClass tempDiscipleClass = new BaseDiscipleClass();
                return tempDiscipleClass.characterClassDescription;
            case 4:
                BaseCharacterClass tempArcaneBladeClass = new BaseArcaneBladeClass();
                return tempArcaneBladeClass.characterClassDescription;
            case 5:
                BaseCharacterClass tempCasterClass = new BaseCasterClass();
                return tempCasterClass.characterClassDescription;
            case 6:
                BaseCharacterClass tempEBBMancerClass = new BaseEBBMancerClass();
                return tempEBBMancerClass.characterClassDescription;
            case 7:
                BaseCharacterClass tempDivinerClass = new BaseDivinerClass();
                return tempDivinerClass.characterClassDescription;
            case 8:
                BaseCharacterClass tempWilderClass = new BaseWilderClass();
                return tempWilderClass.characterClassDescription;
            case 9:
                BaseCharacterClass tempDevastatorClass = new BaseDevastatorClass();
                return tempDevastatorClass.characterClassDescription;
            case 10:
                BaseCharacterClass tempInterfacerClass = new BaseInterfacerClass();
                return tempInterfacerClass.characterClassDescription;
            case 11:
                BaseCharacterClass tempNovaClass = new BaseNovaClass();
                return tempNovaClass.CharacterClassDescription;
        }

        return "No Class Selected";
	}

	 //public void FindRaceDescription(int raceSelection) {
  //      switch (raceSelection)
  //      {
  //          case 0:
  //              BaseCharacterRace tempRace = new BaseTaenRace();
  //              return tempRace.RaceDescription;
  //          case 1:
  //              BaseCharacterRace tempRace = new BaseMaroMaroRace();
  //              return tempRace.RaceDescription;
  //          case 2:
  //              BaseCharacterRace tempRace = new BaseDraazenRace();
  //              return tempRace.RaceDescription;
  //          case 3:
  //              BaseCharacterRace tempRace = new BaseWeyevRace();
  //              return tempRace.RaceDescription;
  //          case 4:
  //              BaseCharacterRace tempRace = new BaseTrelkanRace();
  //              return tempRace.RaceDescription;
  //          case 5:
  //              BaseCharacterRace tempRace = new BaseShalnickRace();
  //              return tempRace.RaceDescription;
  //          case 6:
  //              BaseCharacterRace tempRace = new BaseHaffenvalkRace();
  //              return tempRace.RaceDescription;
  //      }

  //      return "No Race Selected";

	 //}

	public string FindClassStatValue (int classSelection ) {
        switch (classSelection)
        {
            case 0:
                BaseCharacterClass tempHeraldClass = new BaseHeraldClass();
                string tempHeraldClassStats = "Will " + tempHeraldClass.Will + "\n" + "Essence " + tempHeraldClass.Essence + "\n" + "Speed " + tempHeraldClass.Speed + "\n" + "Mind " + tempHeraldClass.Mind + "\n" + "Strength " + tempHeraldClass.Strength + "\n" + "Soul " + tempHeraldClass.Soul + "\n" + "Ultima " + tempHeraldClass.Ultima + "\n" + "Body " + tempHeraldClass.Body;
                return tempHeraldClassStats;
            case 1:
                BaseCharacterClass tempBlazerClass = new BaseBlazerClass();
                string tempBlazerClassStats = "Will " + tempBlazerClass.Will + "\n" + "Essence " + tempBlazerClass.Essence + "\n" + "Speed " + tempBlazerClass.Speed + "\n" + "Mind " + tempBlazerClass.Mind + "\n" + "Strength " + tempBlazerClass.Strength + "\n" + "Soul " + tempBlazerClass.Soul + "\n" + "Ultima " + tempBlazerClass.Ultima + "\n" + "Body " + tempBlazerClass.Body;
                return tempBlazerClassStats;
            case 2:
                BaseCharacterClass tempDeviantClass = new BaseDeviantClass();
                string tempDeviantClassStats = "Will " + tempDeviantClass.Will + "\n" + "Essence " + tempDeviantClass.Essence + "\n" + "Speed " + tempDeviantClass.Speed + "\n" + "Mind " + tempDeviantClass.Mind + "\n" + "Strength " + tempDeviantClass.Strength + "\n" + "Soul " + tempDeviantClass.Soul + "\n" + "Ultima " + tempDeviantClass.Ultima + "\n" + "Body " + tempDeviantClass.Body;
                return tempDeviantClassStats;
            case 3:
                BaseCharacterClass tempDiscipleClass = new BaseDiscipleClass();
                string tempDiscipleClassStats = "Will " + tempDiscipleClass.Will + "\n" + "Essence " + tempDiscipleClass.Essence + "\n" + "Speed " + tempDiscipleClass.Speed + "\n" + "Mind " + tempDiscipleClass.Mind + "\n" + "Strength " + tempDiscipleClass.Strength + "\n" + "Soul " + tempDiscipleClass.Soul + "\n" + "Ultima " + tempDiscipleClass.Ultima + "\n" + "Body " + tempDiscipleClass.Body;
                return tempDiscipleClassStats;
            case 4:
                BaseCharacterClass tempArcaneBladeClass = new BaseArcaneBladeClass();
                string tempArcaneBladeClassStats = "Will " + tempArcaneBladeClass.Will + "\n" + "Essence " + tempArcaneBladeClass.Essence + "\n" + "Speed " + tempArcaneBladeClass.Speed + "\n" + "Mind " + tempArcaneBladeClass.Mind + "\n" + "Strength " + tempArcaneBladeClass.Strength + "\n" + "Soul " + tempArcaneBladeClass.Soul + "\n" + "Ultima " + tempArcaneBladeClass.Ultima + "\n" + "Body " + tempArcaneBladeClass.Body;
                return tempArcaneBladeClassStats;
            case 5:
                BaseCharacterClass tempCasterClass = new BaseCasterClass();
                string tempCasterClassStats = "Will " + tempCasterClass.Will + "\n" + "Essence " + tempCasterClass.Essence + "\n" + "Speed " + tempCasterClass.Speed + "\n" + "Mind " + tempCasterClass.Mind + "\n" + "Strength " + tempCasterClass.Strength + "\n" + "Soul " + tempCasterClass.Soul + "\n" + "Ultima " + tempCasterClass.Ultima + "\n" + "Body " + tempCasterClass.Body;
                return tempCasterClassStats;
            case 6:
                BaseCharacterClass tempEBBMancerClass = new BaseEBBMancerClass();
                string tempEBBMancerClassStats = "Will " + tempEBBMancerClass.Will + "\n" + "Essence " + tempEBBMancerClass.Essence + "\n" + "Speed " + tempEBBMancerClass.Speed + "\n" + "Mind " + tempEBBMancerClass.Mind + "\n" + "Strength " + tempEBBMancerClass.Strength + "\n" + "Soul " + tempEBBMancerClass.Soul + "\n" + "Ultima " + tempEBBMancerClass.Ultima + "\n" + "Body " + tempEBBMancerClass.Body;
                return tempEBBMancerClassStats;
            case 7:
                BaseCharacterClass tempDivinerClass = new BaseDivinerClass();
                string tempDivinerClassStats = "Will " + tempDivinerClass.Will + "\n" + "Essence " + tempDivinerClass.Essence + "\n" + "Speed " + tempDivinerClass.Speed + "\n" + "Mind " + tempDivinerClass.Mind + "\n" + "Strength " + tempDivinerClass.Strength + "\n" + "Soul " + tempDivinerClass.Soul + "\n" + "Ultima " + tempDivinerClass.Ultima + "\n" + "Body " + tempDivinerClass.Body;
                return tempDivinerClassStats;
            case 8:
                BaseCharacterClass tempWilderClass = new BaseWilderClass();
                string tempWilderClassStats = "Will " + tempWilderClass.Will + "\n" + "Essence " + tempWilderClass.Essence + "\n" + "Speed " + tempWilderClass.Speed + "\n" + "Mind " + tempWilderClass.Mind + "\n" + "Strength " + tempWilderClass.Strength + "\n" + "Soul " + tempWilderClass.Soul + "\n" + "Ultima " + tempWilderClass.Ultima + "\n" + "Body " + tempWilderClass.Body;
                return tempWilderClassStats;
            case 9:
                BaseCharacterClass tempDevastatorClass = new BaseDevastatorClass();
                string tempDevastatorClassStats = "Will " + tempDevastatorClass.Will + "\n" + "Essence " + tempDevastatorClass.Essence + "\n" + "Speed " + tempDevastatorClass.Speed + "\n" + "Mind " + tempDevastatorClass.Mind + "\n" + "Strength " + tempDevastatorClass.Strength + "\n" + "Soul " + tempDevastatorClass.Soul + "\n" + "Ultima " + tempDevastatorClass.Ultima + "\n" + "Body " + tempDevastatorClass.Body;
                return tempDevastatorClassStats;
            case 10:
                BaseCharacterClass tempInterfacerClass = new BaseInterfacerClass();
                string tempInterfacerClassStats = "Will " + tempInterfacerClass.Will + "\n" + "Essence " + tempInterfacerClass.Essence + "\n" + "Speed " + tempInterfacerClass.Speed + "\n" + "Mind " + tempInterfacerClass.Mind + "\n" + "Strength " + tempInterfacerClass.Strength + "\n" + "Soul " + tempInterfacerClass.Soul + "\n" + "Ultima " + tempInterfacerClass.Ultima + "\n" + "Body " + tempInterfacerClass.Body;
                return tempInterfacerClassStats;
            case 11:
                BaseCharacterClass tempNovaClass = new BaseNovaClass();
                string tempNovaClassStats = "Will " + tempNovaClass.Will + "\n" + "Essence " + tempNovaClass.Essence + "\n" + "Speed " + tempNovaClass.Speed + "\n" + "Mind " + tempNovaClass.Mind + "\n" + "Strength " + tempNovaClass.Strength + "\n" + "Soul " + tempNovaClass.Soul + "\n" + "Ultima " + tempNovaClass.Ultima + "\n" + "Body " + tempNovaClass.Body;
                return tempNovaClassStats;
        }

        return "NO STATS FOUND";
	}



	public void DisplayStatAllocation() {
		// list of stats with plus and minus button
		// prevent user from add more stats then given
		statAllocationModule.DisplayStatAllocationModule();
		
	}

	public void DisplayFinalSetup() {
		// Name
		playerName = GUI.TextArea(new Rect(20, 10, 100, 35), playerName, 25);
		// Gender
		genderSelection = GUI.SelectionGrid(new Rect(20,265,150,55), genderSelection, genderTypes, 2);
		// Discription
		playerBio = GUI.TextArea(new Rect(20, 50, 250, 200), playerBio, 250);

	}

	public void ChooseClass(int classSelection) {
        switch (classSelection)
        {
            case 0:
                GameInformation.CharacterClassName = new BaseHeraldClass();
                break;
            case 1:
                GameInformation.CharacterClassName = new BaseBlazerClass();
                break;
            case 2:
                GameInformation.CharacterClassName = new BaseDeviantClass();
                break;
            case 3:
                GameInformation.CharacterClassName = new BaseDiscipleClass();
                break;
            case 4:
                GameInformation.CharacterClassName = new BaseArcaneBladeClass();
                break;
            case 5:
                GameInformation.CharacterClassName = new BaseCasterClass();
                break;
            case 6:
                GameInformation.CharacterClassName = new BaseEBBMancerClass();
                break;
            case 7:
                GameInformation.CharacterClassName = new BaseDivinerClass();
                break;
            case 8:
                GameInformation.CharacterClassName = new BaseWilderClass();
                break;
            case 9:
                GameInformation.CharacterClassName = new BaseDevastatorClass();
                break;
            case 10:
                GameInformation.CharacterClassName = new BaseInterfacerClass();
                break;
            case 11:
                GameInformation.CharacterClassName = new BaseNovaClass();
                break;
        }
    }

	public void DisplayMainItems() {
		GUI.Label(new Rect(Screen.width/2, 20, 250, 100), "Create New Player");
		
		if(CreateAPlayerGUI.currentState != CreateAPlayerGUI.CreateAPlayerStates.FINALSETUP) { // If not in final setup then show a next button
			if(GUI.Button(new Rect(470, 370, 50 ,50), "Next"))
            
            {
				if(CreateAPlayerGUI.currentState == CreateAPlayerGUI.CreateAPlayerStates.CLASSSELECTION)
                {
					ChooseClass(classSelection);
					CreateAPlayerGUI.currentState = CreateAPlayerGUI.CreateAPlayerStates.STATALLOCATION;
				}

                else if (CreateAPlayerGUI.currentState == CreateAPlayerGUI.CreateAPlayerStates.STATALLOCATION)
                
                {
					GameInformation.Will = statAllocationModule.pointsToAllocate[0];
					GameInformation.Mind = statAllocationModule.pointsToAllocate[1];
					GameInformation.Body = statAllocationModule.pointsToAllocate[2];
					GameInformation.Soul = statAllocationModule.pointsToAllocate[3];
					GameInformation.Ultima = statAllocationModule.pointsToAllocate[4];
					GameInformation.Strength = statAllocationModule.pointsToAllocate[5];
					GameInformation.Essence = statAllocationModule.pointsToAllocate[6];
					GameInformation.Speed = statAllocationModule.pointsToAllocate[7];
					CreateAPlayerGUI.currentState = CreateAPlayerGUI.CreateAPlayerStates.FINALSETUP;
				}
			}
		} else if(CreateAPlayerGUI.currentState == CreateAPlayerGUI.CreateAPlayerStates.FINALSETUP) {
			if(GUI.Button(new Rect(470, 370, 50 ,50), "Finish")) {
                // FINAL SAVE
                GameInformation.PlayerName = playerName;
                if (genderSelection == 0)
                {
                    GameInformation.IsMale = true;    
                }
                else if(genderSelection == 1)
                {
                    GameInformation.IsMale = false;
                }

				SaveInformation.SaveAllInformation();
				Debug.Log ("Make Final Save");
			}
		}

		if(CreateAPlayerGUI.currentState != CreateAPlayerGUI.CreateAPlayerStates.CLASSSELECTION) {
			if(GUI.Button(new Rect(295, 370, 50 ,50), "Back")) {
				if(CreateAPlayerGUI.currentState == CreateAPlayerGUI.CreateAPlayerStates.STATALLOCATION) {
					statAllocationModule.didRunOnce = false;
                    GameInformation.CharacterClassName = null;
					CreateAPlayerGUI.currentState = CreateAPlayerGUI.CreateAPlayerStates.CLASSSELECTION;
				} else if (CreateAPlayerGUI.currentState == CreateAPlayerGUI.CreateAPlayerStates.FINALSETUP){
					CreateAPlayerGUI.currentState = CreateAPlayerGUI.CreateAPlayerStates.STATALLOCATION;
				}
			}
		}


	}	
}
