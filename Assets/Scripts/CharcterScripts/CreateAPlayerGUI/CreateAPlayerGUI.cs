﻿using UnityEngine;

public class CreateAPlayerGUI : MonoBehaviour {

	public enum CreateAPlayerStates {
		CLASSSELECTION, // Display All Classes
		RACESELECTION, // Allocate stats where the player wants too
		STATALLOCATION, // add name and misc items
		FINALSETUP
	}

    private readonly DisplayCreatePlayerFunctions displayFunction = new DisplayCreatePlayerFunctions();

    public static CreateAPlayerStates currentState;


	// Use this for initialization
	void Start () {
		currentState = CreateAPlayerStates.CLASSSELECTION;
	}
	
	// Update is called once per frame
	void Update () {
		switch (currentState){
			case(CreateAPlayerStates.CLASSSELECTION):
				break;
			//case(CreateAPlayerStates.RACESELECTION):
				//break;
			case(CreateAPlayerStates.STATALLOCATION):
				break;
			case(CreateAPlayerStates.FINALSETUP):
				break;
		}
	}

	void OnGUI () {

		displayFunction.DisplayMainItems();

		if(currentState == CreateAPlayerStates.CLASSSELECTION) {
			// Display Class Selection Fuction
			displayFunction.DisplayClassSelections();
		}
		// if(currentState == CreateAPlayerStates.RACESELECTION) {
		// 	// Display Class Selection Fuction
		// 	displayFunction.DisplayRaceSelections();
		// }
		if(currentState == CreateAPlayerStates.STATALLOCATION) {
			// Display Class Selection Fuction
			displayFunction.DisplayStatAllocation();
		}

		if(currentState == CreateAPlayerStates.FINALSETUP) {
			// Display Class Selection Fuction
			displayFunction.DisplayFinalSetup();
			Debug.Log(GameInformation.Will);
		}
	}
}
