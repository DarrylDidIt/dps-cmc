﻿using UnityEngine;

public class StatAllocationModule {

	private string[] statNames = { 
		"Strength",		    // 1
		"Essence",		    // 2
		"Will",		        // 3
		"Speed",		    // 4
		"Mind",	            // 5
		"Body",		        // 6
		"Ultima",	        // 7
		"Soul",	            // 8
	};
	private string[] statDescription = { 
		"Increase Attack",	// 1
		"Increase Essence",	// 2
		"Increase Will",	// 3
		"Increase Speed",	// 4
		"Increase Mind",	// 5
		"Increase Body",	// 6
		"Increase Ultima",	// 7
		"Increase Soul",	// 8
	};
	private bool[] statSelection = new bool[8];
	
	public int[] pointsToAllocate = new int[8]; // Starting stat values for the chosen class.
    private readonly int[] baseStatPoints = new int[8]; //Starting stat values for the chosen class.

    private int availPoints = 5;
	public bool didRunOnce;

	public void DisplayStatAllocationModule() {
		if(!didRunOnce) {
			RetrieveStatBaseStatPoints();
			didRunOnce = true;
		}
		DisplayStatToggleSwitches();
		DisplayStatIncreaseDecreaseButtons();

	}

	private void DisplayStatToggleSwitches() {
		for(int i = 0; i < statNames.Length; i++) {
			statSelection[i] = GUI.Toggle(new Rect(10, 60*i + 10,100,50), statSelection[i], statNames[i]);
			GUI.Label(new Rect(100, 60*i + 10,50,50), pointsToAllocate[i].ToString());
			GUI.Label(new Rect(1000,60*i + 10,50,50), pointsToAllocate[i].ToString());
			if(statSelection[i]) {
				GUI.Label(new Rect(20,60*i + 30,150,100), statDescription[i]);
			}
		}
	}

	private void DisplayStatIncreaseDecreaseButtons() {
		for (int i = 0; i < pointsToAllocate.Length; i++) {
			if (pointsToAllocate [i] >= baseStatPoints [i] && availPoints > 0) {
				if(GUI.Button(new Rect(200,60*i + 10,50,50), "+")) {
					pointsToAllocate[i] += 1;
					--availPoints;
				}
			}
			if(pointsToAllocate[i] > baseStatPoints[i]) {
				if(GUI.Button(new Rect(260,60*i + 10,50,50), "-")) {
					pointsToAllocate[i] -= 1;
					++availPoints;
				}
			}
			
		}
	}

	private void RetrieveStatBaseStatPoints() {
        BaseCharacterClass cClass = GameInformation.CharacterClassName;
		pointsToAllocate[0] = cClass.Strength;
		baseStatPoints[0] = cClass.Strength;
		pointsToAllocate[1] = cClass.Essence;
		baseStatPoints[1] = cClass.Essence;
		pointsToAllocate[2] = cClass.Will;
		baseStatPoints[2] = cClass.Will;
		pointsToAllocate[3] = cClass.Speed;
		baseStatPoints[3] = cClass.Speed;
		pointsToAllocate[4] = cClass.Mind;
		baseStatPoints[4] = cClass.Mind;
		pointsToAllocate[5] = cClass.Body;
		baseStatPoints[5] = cClass.Body;
		pointsToAllocate[6] = cClass.Ultima;
		baseStatPoints[6] = cClass.Ultima;
		pointsToAllocate[7] = cClass.Soul;
		baseStatPoints[7] = cClass.Soul;
	}
}
