﻿using UnityEngine;

public class QuitOnClick : MonoBehaviour {
	public void quit() {
        //#if Unity_Enity
        UnityEditor.EditorApplication.isPlaying = false;
        //#false
        Application.Quit();
	}
}
