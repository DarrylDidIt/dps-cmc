﻿[System.Serializable]
public class BasicAttackAbility : BaseAbility {
    public BasicAttackAbility() {
        AbilityName = "Basic Attack";
        AbilityDescription = "A Basic Attack";
        AbilityID = 1;
        AbilityPower = 10;
        AbilityCost = 0;
    }
}
