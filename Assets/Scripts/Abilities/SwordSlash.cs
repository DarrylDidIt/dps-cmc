﻿[System.Serializable]
public class SwordSlash : BaseAbility {
    public SwordSlash() {
        AbilityName = "Sword Slash Attack";
        AbilityDescription = "A Strong Slash";
        AbilityID = 2;
        AbilityPower = 30;
        AbilityCost = 10;
    }
}
