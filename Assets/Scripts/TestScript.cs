﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    private BaseCharacterRace class1;
    private BaseCharacterClass class2;

    // Use this for initialization
    protected void Start()
    {
        class2 = new BaseHeraldClass();

        Debug.Log(class2.CharacterClassName);
        Debug.Log(class2.characterClassDescription);
        Debug.Log(class2.mind);
        Debug.Log(class2.essence);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
