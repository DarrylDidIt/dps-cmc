﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStateMachine : MonoBehaviour {

    private BattleStateMachine _BSM;
	public BaseEnemy enemy;

    public enum TurnState
    {
        PROCESSING,
        CHOOSEACTION,
        WAITING,
        ACTION,
        DEAD
    }

    public TurnState currentState;

    //ProgressBar
    private float cur_cooldown;
    [SerializeField]
    private float max_cooldown = 7f;
    public Image ProgressBar;

    // Starting position
    private Vector3 startPosition;

    //TImefor action
    private bool actionStarted = false;
    [SerializeField]
    private float animSpeed = 5f;

    public GameObject heroToAttack;
    // Use this for initialization
    void Start () {
        currentState = TurnState.PROCESSING;


        _BSM = GameObject.Find("BettleManager").GetComponent<BattleStateMachine>();
        startPosition = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(currentState);
        switch (currentState)
        {
            case (TurnState.PROCESSING):
                UpgreadeProgressBar();
                break;

            case (TurnState.CHOOSEACTION):
                ChooseAction();
                currentState = TurnState.WAITING;
                break;

            case (TurnState.WAITING):

                break;

            case (TurnState.ACTION):
                StartCoroutine(TimeForAction());
                break;

            case (TurnState.DEAD):

                break;
        }
    }

    private void UpgreadeProgressBar()
    {
        cur_cooldown = cur_cooldown + Time.deltaTime;
        float cal_coolDown = cur_cooldown / max_cooldown;

        ProgressBar.transform.localScale = new Vector3(Mathf.Clamp(cal_coolDown, 0, 1), ProgressBar.transform.localScale.y, ProgressBar.transform.localScale.z);

        if (cur_cooldown >= max_cooldown)
        {
            currentState = TurnState.CHOOSEACTION;
        }

    }

    void ChooseAction()
    {
        HandleTurns myAttack = new HandleTurns();
        myAttack.Attacker = this.gameObject.name;
        myAttack.Type = "Enemy";
        myAttack.AttackersGameObject = this.gameObject;
        myAttack.AttackersTarget = _BSM.HerosInBattle[Random.Range(0, _BSM.HerosInBattle.Count)];
        _BSM.CollectingAction(myAttack);
    }

    private IEnumerator TimeForAction()
    {
        if (actionStarted)
        {
            yield break;
        }

        actionStarted = true;

        //animate enimey to choosen target
        Vector3 heroPosition = new Vector3(heroToAttack.transform.position.x+1.5f, heroToAttack.transform.position.y, heroToAttack.transform.position.z);
        while(MoveToEnemy(heroPosition))
        {
            yield return null;
        }

        // wait
        yield return new WaitForSeconds(0.5f);
        // do damage

        //return to startPotion
        Vector3 firstPosition = startPosition;
        while (MoveToStart(firstPosition)){yield return null;}

        // remove this performer from the list in BSM
        _BSM.PerformList.RemoveAt(0);
        //reset BSM -> WAIT
        _BSM.battleStates = BattleStateMachine.PerformAction.WAIT;
        //End Coroutine
        actionStarted = false;
        //reset this enemy state
        cur_cooldown = 0f;
        currentState = TurnState.PROCESSING;
    }

    private bool MoveToEnemy(Vector3 target)
    {
        return target != (transform.position = Vector3.MoveTowards(transform.position, target, animSpeed * Time.deltaTime));
    }
    private bool MoveToStart(Vector3 target)
    {
        return target != (transform.position = Vector3.MoveTowards(transform.position, target, animSpeed * Time.deltaTime));
    }
}
