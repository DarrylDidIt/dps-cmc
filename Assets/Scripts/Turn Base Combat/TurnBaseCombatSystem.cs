﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnBaseCombatSystem : MonoBehaviour {

	private bool hasAddedXP = false;
	private BattleStateStart battleStateStartScript = new BattleStateStart ();
    private BattleCalculations battleCalcScript = new BattleCalculations();
    public static BaseAbility playerUsedAbility;

	public enum BattleStates {
		// PLayer Animate,
		START,
		//Story Dialogue,
		PLAYERCHOICE,
		ENEMYCHOICE,
		CALCMDAMAGE,
		ADDSTSTUSEFFECT,
		LOSE,
		WIN
	}

   public static BattleStates currentState;

    // Use this for initialization
    void Start () {
		hasAddedXP = false;
		currentState = BattleStates.START;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(currentState); // Remove this before release
		switch (currentState) {
			case (BattleStates.START):
                //STEUP BATTLE FUNCTION
                //Create Enemy
                battleStateStartScript.PrepareBattle();

				//BattleStateStart.ParpareBattle();
				// Choose who gose first based on speed


				break;
			case (BattleStates.PLAYERCHOICE):
                battleCalcScript.CalculateAbilityDamage(playerUsedAbility);
				break; 

			case (BattleStates.ENEMYCHOICE):
				break;

			case (BattleStates.CALCMDAMAGE):
                battleCalcScript.CalculateAbilityDamage(playerUsedAbility);
                Debug.Log("Calculating Damage");
				break;

			case (BattleStates.ADDSTSTUSEFFECT):
				break;



			case (BattleStates.LOSE):
				break; 

			case (BattleStates.WIN):
			  if(!hasAddedXP){
			 	IncreaseExperience.AddExperience();
			  	hasAddedXP = true;
			  }
			
				break; 
		}
	}

	//void OnGUI() {
	//	if(GUILayout.Button("Battle Start!")) {
 //           switch (currentState)
 //           {
 //               case BattleStates.START:
 //                   currentState = BattleStates.PLAYERCHOICE;
 //                   break;
 //               case BattleStates.PLAYERCHOICE:
 //                   currentState = BattleStates.ENEMYCHOICE;
 //                   break;
 //               case BattleStates.ENEMYCHOICE:
 //                   currentState = BattleStates.LOSE;
 //                   break;
 //               case BattleStates.LOSE:
 //                   currentState = BattleStates.WIN;
 //                   break;
 //               case BattleStates.WIN:
 //                   currentState = BattleStates.START;
 //                   break;
 //           }
 //       }
	//}
}
