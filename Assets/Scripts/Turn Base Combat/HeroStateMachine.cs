﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeroStateMachine : MonoBehaviour
{
    // Get info from BaseHero
    public BaseHero hero;

    //List of Heros states
    public enum TurnState
    {
        PROCESSING,
        ADDTOLIST,
        WAITING,
        SELECTING,
        ACTION,
        DEAD
    }

    public TurnState currentState;

    //ProgressBar
    private float cur_cooldown;
    [SerializeField]
    private float max_cooldown = 5f;
    public Image ProgressBar;


    // Use this for initialization
    void Start()
    {
        currentState = TurnState.PROCESSING;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(currentState);
        switch (currentState)
        {
            case (TurnState.PROCESSING):
                UpgreadeProgressBar();
                break;

            case (TurnState.ADDTOLIST):

                break;

            case (TurnState.WAITING):

                break;

            case (TurnState.SELECTING):
                break;

            case (TurnState.ACTION):

                break;

            case (TurnState.DEAD):

                break;
        }
    }

    private void UpgreadeProgressBar()
    {
        cur_cooldown = cur_cooldown + Time.deltaTime;
        float cal_coolDown = cur_cooldown / max_cooldown;

        ProgressBar.transform.localScale = new Vector3(Mathf.Clamp(cal_coolDown, 0, 1), ProgressBar.transform.localScale.y, ProgressBar.transform.localScale.z);

         if (ProgressBar)
        {
            Debug.Log("No game objects are tagged with 'Enemy'");
        }

        if (cur_cooldown >= max_cooldown)
        {
            currentState = TurnState.ADDTOLIST;
        }

    }
}
