﻿using UnityEngine;
using System.Collections;

public class StatCalculations {
    private float enemyHealthModifier = 0.10f; // 10%
    private float enemyEnergyModifier = 0.10f; // 10%
    private float enemyWillModifier = 0.10f; // 10%
    private float enemyMindModifier = 0.10f; // 10%
    private float enemyStrengthModifier = 0.10f; // 10%
    private float enemyBodyModifier = 0.10f; // 10%
    private float enemySoulModifier = 0.10f; // 10%
    private float enemyEssenceModifier = 0.10f; // 10%
    private float enemySpeedModifier = 0.10f; // 10%
    private float playerBodyModifier = 0.30f; // 30%
    private float playerSoulModifier = 0.30f; // 30%
    private float statModifier;

    public enum StatType {
        Health,
        Energy,
        Will,
        Speed,
        Mind,
        Strength,
        Body,
        Soul,
        Essence
    }

    public int CalculateStat(int statVal, StatType statType, int level, bool isEnemy) 
    {
        if (isEnemy)
        {
            SetEnemyModifier(statType);
            return (statVal + (int)((statVal * statModifier) * level));
        } else if (!isEnemy) {
            SetEnemyModifier(statType);
            return (statVal + (int)((statVal * statModifier) * level));
        }
        return 0;
    }

    private void SetEnemyModifier(StatType statType) {
        if (statType == StatType.Will) {
            statModifier = enemyWillModifier;
        }
        if (statType == StatType.Mind) {
            statModifier = enemyMindModifier;
        }
        if (statType == StatType.Strength) {
            statModifier = enemyStrengthModifier;
        }
        if (statType == StatType.Body) {
            statModifier = enemyBodyModifier;
            statModifier = playerBodyModifier;
        }
        if (statType == StatType.Soul) {
            statModifier = enemySoulModifier;
            statModifier = playerSoulModifier;
        }
        if (statType == StatType.Speed) {
            statModifier = enemyEssenceModifier;
        }
        if (statType == StatType.Essence) {
            statModifier = enemyEssenceModifier;
        }
    }

    public int CalculateHealth(int statValue) {
        return statValue * 100; // CalculateStat base health on total health state * 100
    }

    public int CalculateEnergy(int statValue) {
        return statValue * 50;
    }
}