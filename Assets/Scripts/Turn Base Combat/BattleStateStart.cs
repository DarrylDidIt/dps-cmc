﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStateStart {

	private BasePlayer newEnemy = new BasePlayer ();
    private StatCalculations statCalculations = new StatCalculations ();
    private BaseCharacterClass[] classTypes = new BaseCharacterClass[] { new BaseDevastatorClass(), new BaseArcaneBladeClass(), new BaseBlazerClass(), new BaseCasterClass(), new BaseDeviantClass(), new BaseDiscipleClass(), new BaseDivinerClass(), new BaseEBBMancerClass(), new BaseHeraldClass(), new BaseInterfacerClass(), new BaseNovaClass (), new BaseWilderClass () };
    private string[] enemyName = new string[] { "Common Enemy", "Rare Enemy", "Legendary Enemy", "Exotic Enemy" };

    private int playerBody;
    private int playerSoul;
    private int playerHealth;
    private int playerEnergy;

    //   private float mindModifier = 0.15f; //15%
	//private float strengthModifier = 0.15f; //15%
	//private float essenceModifier = 0.1f; //15%
	//private float speedModifier = 0.1f; //15%
    //private float willModifier = 0.1f; //15%
    //private float bodyModifier = 0.1f; //15%
    //private float soulModifier = 0.1f; //15%

	public void PrepareBattle() {
        // Create Enemy
        CreateNewEnemy();
        DeterminePlayerVitals();
        WhoGoseFirst();

		// choose who gose first based on speed
	}

	 private void CreateNewEnemy() {
        newEnemy.PlayerName = enemyName[Random.Range(0,enemyName.Length)];
	 	newEnemy.PlayerLevel = Random.Range (GameInformation.PlayerLevel - 2, GameInformation.PlayerLevel +2);
        if (newEnemy.PlayerClass != null)
        {
            newEnemy.PlayerClass = classTypes[Random.Range(0, classTypes.Length)];
        }
        newEnemy.mind = statCalculations.CalculateStat (newEnemy.mind, StatCalculations.StatType.Mind, newEnemy.PlayerLevel, true);
        newEnemy.strength = statCalculations.CalculateStat (newEnemy.strength, StatCalculations.StatType.Strength, newEnemy.PlayerLevel, true);
        newEnemy.body = statCalculations.CalculateStat (newEnemy.body, StatCalculations.StatType.Body, newEnemy.PlayerLevel, true);
        newEnemy.speed = statCalculations.CalculateStat (newEnemy.speed, StatCalculations.StatType.Soul, newEnemy.PlayerLevel, true);
        newEnemy.will = statCalculations.CalculateStat (newEnemy.will, StatCalculations.StatType.Will, newEnemy.PlayerLevel, true);
        newEnemy.soul = statCalculations.CalculateStat (newEnemy.soul, StatCalculations.StatType.Soul, newEnemy.PlayerLevel, true);
        newEnemy.essence = statCalculations.CalculateStat (newEnemy.essence, StatCalculations.StatType.Essence, newEnemy.PlayerLevel, true);
        newEnemy.speed = statCalculations.CalculateStat(newEnemy.speed, StatCalculations.StatType.Speed, newEnemy.PlayerLevel, true);
	 }

    private void WhoGoseFirst()
    {
        if(GameInformation.Speed >= newEnemy.speed) {
            TurnBaseCombatSystem.currentState = TurnBaseCombatSystem.BattleStates.PLAYERCHOICE;
        }
        if(GameInformation.Speed < newEnemy.speed) {
            TurnBaseCombatSystem.currentState = TurnBaseCombatSystem.BattleStates.ENEMYCHOICE;
        }
        if(GameInformation.Speed == newEnemy.speed) {
            TurnBaseCombatSystem.currentState = TurnBaseCombatSystem.BattleStates.PLAYERCHOICE;
        }
    }

    private void DeterminePlayerVitals() {
        playerBody = statCalculations.CalculateStat(GameInformation.Body, StatCalculations.StatType.Body, GameInformation.PlayerLevel, false);
        playerSoul = statCalculations.CalculateStat(GameInformation.Soul, StatCalculations.StatType.Soul, GameInformation.PlayerLevel, false);
        playerHealth = statCalculations.CalculateHealth(playerBody);
        playerEnergy = statCalculations.CalculateEnergy(playerSoul);
        GameInformation.PlayerHealth = playerHealth;
        GameInformation.PlayerErengy = playerEnergy;
    }
}
