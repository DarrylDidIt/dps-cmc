﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleStateMachine : MonoBehaviour {

    public enum PerformAction
    {
        WAIT,
        TAKEACTION,
        PERFORMACTION,
    }

    public PerformAction battleStates;


    public List<HandleTurns> PerformList = new List<HandleTurns> ();

    public List<GameObject> HerosInBattle = new List<GameObject>();
    public List<GameObject> EnemiesInBattle = new List<GameObject> ();

	// Use this for initialization
	void Start () {
        battleStates = PerformAction.WAIT;

        GameObject[] gameObjects;
        gameObjects = GameObject.FindGameObjectsWithTag("Enemy");
        gameObjects = GameObject.FindGameObjectsWithTag("Hero");

        EnemiesInBattle = new List<GameObject>(gameObjects);
        HerosInBattle = new List<GameObject>(gameObjects);
    }
	
	// Update is called once per frame
	void Update () 
    {
	    switch(battleStates)
        {
            case(PerformAction.WAIT):
                if(PerformList.Count > 0)
                {
                    battleStates = PerformAction.TAKEACTION;
                }
                break;
            case (PerformAction.TAKEACTION):
                GameObject performer = GameObject.Find(PerformList[0].Attacker);

                if(PerformList[0].Type == "Enemy")
                {
                    EnemyStateMachine _ESM = performer.GetComponent<EnemyStateMachine>();
                    _ESM.heroToAttack = PerformList[0].AttackersTarget;
                    _ESM.currentState = EnemyStateMachine.TurnState.ACTION;
                }

                if (PerformList[0].Type == "Hero")
                {

                }
                battleStates = PerformAction.PERFORMACTION;
                break;
            case (PerformAction.PERFORMACTION):
                break;
        }
	}

    public void CollectingAction(HandleTurns input)
    {
        PerformList.Add(input);
    }
}
