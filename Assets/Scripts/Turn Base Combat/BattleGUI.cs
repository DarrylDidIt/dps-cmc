﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleGUI : MonoBehaviour {
    private string playerName;
    private int playerLevel;
    private int playerHealth;
    private int PlayerEnergy;

	// Use this for initialization
	void Start () {
        playerName = GameInformation.PlayerName;
        playerLevel = GameInformation.PlayerLevel;

		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnGUI() {
        if(TurnBaseCombatSystem.currentState == TurnBaseCombatSystem.BattleStates.PLAYERCHOICE) {
            DisplayPLayerChoice();
        }

        //Show enemy health
        //Player information
        
	}

    private void DisplayPLayerChoice() {
        if (GUI.Button(new Rect(Screen.width - 250, Screen.height - 50, 100, 30), GameInformation.playerMoveOne.AbilityName))
        {
            TurnBaseCombatSystem.playerUsedAbility = GameInformation.playerMoveOne;
            TurnBaseCombatSystem.currentState = TurnBaseCombatSystem.BattleStates.CALCMDAMAGE;
        }
        if (GUI.Button(new Rect(Screen.width - 150, Screen.height - 50, 100, 30), GameInformation.playerMoveTwo.AbilityName)){
            TurnBaseCombatSystem.currentState = TurnBaseCombatSystem.BattleStates.CALCMDAMAGE;
        }   
    }
}
