﻿using UnityEngine;

namespace SavingAndLoading
{
    public class SaveInformation 
    {

        public static void SaveAllInformation()
        {
            PlayerPrefs.SetInt("PLAYERLEVEL", GameInformation.PlayerLevel);
            PlayerPrefs.SetString("PLAYERNAME", GameInformation.PlayerName);

            PlayerPrefs.SetInt("MIND", GameInformation.Mind);
            PlayerPrefs.SetInt("STRENGTH", GameInformation.Strength);
            PlayerPrefs.SetInt("ESSENCE", GameInformation.Essence);
            PlayerPrefs.SetInt("WILL", GameInformation.Will);
            PlayerPrefs.SetInt("BODY", GameInformation.Strength);
            PlayerPrefs.SetInt("SOUL", GameInformation.Soul);
            PlayerPrefs.SetInt("ULTIMA", GameInformation.Ultima);
            PlayerPrefs.SetInt("GOLD", GameInformation.Gold);

            if (GameInformation.EquipmentOne != null)
            {
                PPSerialization.Save("Equipment Item1", GameInformation.EquipmentOne);
            }

            Debug.Log("All Information Saved");
        }
    }
}
