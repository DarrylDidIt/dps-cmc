﻿using UnityEngine;
using SavingAndLoading;

public static class LoadInformation
{

    static void LoadAllInformation()
    {
        GameInformation.PlayerName = PlayerPrefs.GetString("Player Name");
        GameInformation.PlayerLevel = PlayerPrefs.GetInt("Player Level");
        GameInformation.Mind = PlayerPrefs.GetInt("Mind");
        GameInformation.Strength = PlayerPrefs.GetInt("Strength");
        GameInformation.Essence = PlayerPrefs.GetInt("Essence");
        GameInformation.Will = PlayerPrefs.GetInt("Will");
        GameInformation.Speed = PlayerPrefs.GetInt("Speed");
        GameInformation.Body = PlayerPrefs.GetInt("Body");
        GameInformation.Soul = PlayerPrefs.GetInt("Soul");
        GameInformation.Ultima = PlayerPrefs.GetInt("Ultima");
        GameInformation.Gold = PlayerPrefs.GetInt("Gold");

        if (PlayerPrefs.GetString("Equipment Item1") != null)
        {
            GameInformation.EquipmentOne = (BaseEquipment)PPSerialization.Load("Equipment Item1");
        }
    }

}
