﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BaseEquipment : BaseStatItem {

	public enum EquipmentTypes {
		HEAD,
		CHEST,
		LEGS
	}

	private EquipmentTypes equipmentType;
	private  int spellEffectID;

	public EquipmentTypes EquipmentType {get;set;}
	public int SpellEffectID {get;set;}

}
