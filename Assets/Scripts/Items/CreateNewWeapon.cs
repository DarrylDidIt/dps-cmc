﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateNewWeapon : MonoBehaviour {

	private BaseWeapon newWeapon;

	void Start (){
		CreateWeapon();

		Debug.Log(newWeapon.ItemName);
		Debug.Log(newWeapon.ItemDescription);
		Debug.Log(newWeapon.ItemID.ToString());
		Debug.Log(newWeapon.Accuracy.ToString());
		Debug.Log(newWeapon.Strength.ToString());
		Debug.Log(newWeapon.Magic.ToString());
		Debug.Log(newWeapon.PhysicalDefence.ToString());
		Debug.Log(newWeapon.MagicalDefence.ToString());
		Debug.Log(newWeapon.Speed.ToString());
	}

	public void CreateWeapon() {
		
		newWeapon = new BaseWeapon();

		//Assign Weapon 
		newWeapon.ItemName = "W" + Random.Range(1,101);
		//Creat a weapon description
		newWeapon.ItemDescription = "This is a Weapon";
		// Weapon ID
		newWeapon.ItemID = Random.Range(1,101);
		//Weapon stats
		// Figure out stat mod**
		newWeapon.Accuracy = Random.Range(1,101);
		newWeapon.Strength = Random.Range(1,101);
		newWeapon.Magic = Random.Range(1,101);
		newWeapon.Speed = Random.Range(1,101);
		newWeapon.PhysicalDefence = Random.Range(1,101);
		newWeapon.MagicalDefence = Random.Range(1,101);
		//Choose type of weapon
		ChooseWeaponType();
		//Spell Effect ID
		newWeapon.SpellEffectID = Random.Range(1,101);
	}

	private void ChooseWeaponType() {
		int randomTemp = Random.Range(0,5);
		
		if (randomTemp == 0) {
			newWeapon.WeaponType = BaseWeapon.WeaponTypes.SWORD;
		} else if (randomTemp == 1) {
			newWeapon.WeaponType = BaseWeapon.WeaponTypes.WAND;
		} else if (randomTemp == 2) {
			newWeapon.WeaponType = BaseWeapon.WeaponTypes.DAGGER;
		} else if (randomTemp == 3) {
			newWeapon.WeaponType = BaseWeapon.WeaponTypes.GUN;
		} else if (randomTemp == 4) {
			newWeapon.WeaponType = BaseWeapon.WeaponTypes.SHEILD;
		}
	}
}
