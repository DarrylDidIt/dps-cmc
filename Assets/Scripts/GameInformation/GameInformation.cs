﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameInformation : MonoBehaviour {

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
    }

    public static List<BaseAbility> playerAbilities;

	public static bool IsMale{get;set;}
	public static BaseEquipment EquipmentOne {get;set;} // Armor and/or Gear
    public static string PlayerName{get;set;} // Player Name
    public static BaseCharacterClass CharacterClassName { get; set; } // Player Class
	public static int PlayerLevel{get;set;} // Player Level
	public static int Strength{get;set;} // PDPS Phyhsical Damage Per Second
	public static int Essence{get;set;}	// RPDS Magical/Range Damage Per Second
	public static int Speed{get;set;} // How offtern a character attacks
	public static int Mind{get;set;} // Acc 3%, Contral 5%, Stratigies 1%
	public static int Will{get;set;} // Reduces Phyical damge 1. Enemy Contral 3%
	public static int Body{get;set;} // Charcter Health
	public static int Soul{get;set;} // Acc 3%, Contral 5%, Stratigies 1% || Mana
	public static int Ultima{get;set;} // Reduces Phyical damge 1. Enemy Contral 3%
	public static int CurrentXP{get;set;} // Reduces Phyical damge 1. Enemy Contral 3%
	public static int RequiredXP{get;set;} // Reduces Phyical damge 1. Enemy Contral 3%
	public static int Gold{get;set;} // in game currency

    public static BaseAbility playerMoveOne = new BasicAttackAbility();
    public static BaseAbility playerMoveTwo = new SwordSlash();

    public static int PlayerHealth { get; set; }
    public static int PlayerErengy { get; set; }
}
