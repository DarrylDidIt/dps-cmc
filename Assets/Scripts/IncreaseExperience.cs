﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IncreaseExperience {

	private static int xpToGive;
	private static LevelUp levelupscript = new LevelUp();


	 public static void AddExperience() {
	 	xpToGive = GameInformation.PlayerLevel * 100; // What evere our player level is * 100 will give us our XP gain.
	 	GameInformation.CurrentXP += xpToGive;
	 	CheckedToSeeIfPlayerLeveled();
	 	Debug.Log( xpToGive );
	 }

	 public static void AddExperienceFromBattleLoss() {
	 	xpToGive = GameInformation.PlayerLevel * 10;
	 	GameInformation.CurrentXP += xpToGive;
	 	CheckedToSeeIfPlayerLeveled();
	 	Debug.Log( xpToGive );
	 }

	 private static void CheckedToSeeIfPlayerLeveled() {
	 	if(GameInformation.CurrentXP >= GameInformation.RequiredXP) {
	 		//the player has leveled
	 		levelupscript.LevelUpCharacter();
	 		// Creat level up script
	 	}
	 }
}
